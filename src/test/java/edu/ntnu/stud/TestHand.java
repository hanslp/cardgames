package edu.ntnu.stud;

import edu.ntnu.stud.model.Hand;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestHand {

    @Test
    void testHandSize() {
        Hand hand = new Hand(5);
        int handSize = hand.getSize();
        assertEquals(5, handSize);
    }

    @Test
    void testHand() {
        Hand hand = new Hand(5);
        Hand hand2 = new Hand(5);
        assertNotEquals(hand, hand2);
    }

    @Test
    void testEventualFlush() {
        int handsDealt = 0;
        Hand hand = new Hand(5);
        while (!hand.checkFlush()) {
            hand = new Hand(5);
            handsDealt++;
        }
        System.out.println("Number of hands dealt to get a flush: " + handsDealt);
        assertTrue(hand.checkFlush());
    }

    @Test
    void testEventualQueenOfSpades() {
        int handsDealt = 0;
        Hand hand = new Hand(5);
        while (!hand.containsQueenOfSpades()) {
            hand = new Hand(5);
            handsDealt++;
        }
        System.out.println("Number of hands dealt to get the Queen of Spades: " + handsDealt);
        assertTrue(hand.containsQueenOfSpades());
    }

    @Test
    void testHeartsInHand() {
        Hand hand = new Hand(52);
        String heartsInHand = hand.getHeartsInHand();
        String expected = "H1, H2, H3, H4, H5, H6, H7, H8, H9, H10, H11, H12, H13";

        assertEquals(expected, heartsInHand);
    }

    @Test
    void testHandSum() {
        Hand hand = new Hand(2);
        int handSum = hand.getHandSum();

        System.out.println("Hand: " + hand.getHandAsString());
        System.out.println("Hand sum: " + handSum);
        assertTrue(handSum > 0);
    }
}
