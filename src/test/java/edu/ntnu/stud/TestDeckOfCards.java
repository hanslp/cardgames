package edu.ntnu.stud;

import edu.ntnu.stud.model.DeckOfCards;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestDeckOfCards {

    @Test
    public void testCardsInDeck() {
        DeckOfCards deck = new DeckOfCards();
        int expectedSize = 52;
        int deckSize = deck.getDeckSize();
        assertEquals(expectedSize, deckSize);
    }

    @Test
    public void testShuffleDeck() {
        DeckOfCards deck = new DeckOfCards();
        DeckOfCards deck2 = new DeckOfCards();
        deck.shuffle();
        assertNotEquals(deck, deck2);
    }
}
