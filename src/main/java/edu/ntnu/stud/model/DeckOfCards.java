package edu.ntnu.stud.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'}; // Spades, Hearts, Diamonds, Clubs
    private final int[] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    private List<PlayingCard> deck;

    public DeckOfCards() {
        deck = new ArrayList<>();
        for (char s : suit) {
            for (int f : face) {
                deck.add(new PlayingCard(s, f));
            }
        }
        shuffle();
    }

    public void shuffle() {
        Collections.shuffle(deck);
    }

    public int getDeckSize() {
        return deck.size();
    }

    // Method to deal a hand of n cards from the deck
    public List<PlayingCard> dealHand(int n) {
        List<PlayingCard> hand = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            int index = random.nextInt(deck.size());
            hand.add(deck.remove(index)); // Remove and add a random card to the hand
        }
        return hand;
    }
}