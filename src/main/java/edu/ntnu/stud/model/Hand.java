package edu.ntnu.stud.model;

import java.util.Comparator;
import java.util.List;

import java.util.stream.Collectors;

public class Hand extends DeckOfCards {
    private List<PlayingCard> hand;
    private int n;

    public Hand(int n) {
        super();
        this.n = n;
        hand = dealHand(n);
    }

    public List<PlayingCard> getHand() {
        return hand;
    }

    public String getHandAsString() {
        return hand.stream()
                .map(PlayingCard::getAsString)
                .collect(Collectors.joining(", "));
    }

    public int getHandSum() {
        return hand.stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    public boolean checkFlush() {
        char suit = hand.getFirst().getSuit();
        return hand.stream()
                .skip(1) // Skip the first card as we already checked its suit
                .allMatch(card -> card.getSuit() == suit);
    }

    public String getHeartsInHand() {
        char suit = 'H';
        return hand.stream()
                .filter(card -> card.getSuit() == suit)
                .sorted(Comparator.comparingInt(PlayingCard::getFace)) // Sort by face value
                .map(PlayingCard::getAsString) // Map each PlayingCard to its string representation
                .collect(Collectors.joining(", ")); // Collect the mapped strings into a single string
    }

    public boolean containsQueenOfSpades() {
        char suit = 'S';
        int face = 12;
        return hand.stream()
                .anyMatch(card -> card.getSuit() == suit && card.getFace() == face);
    }

    public int getSize() {
        return hand.size();
    }
}
