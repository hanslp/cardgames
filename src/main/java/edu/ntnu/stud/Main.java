package edu.ntnu.stud;

import edu.ntnu.stud.view.CardGame;
import javafx.application.Application;

import static javafx.application.Application.launch;

public class Main {
    public static void main(String[] args) {
        Application.launch(CardGame.class, args);
    }
}