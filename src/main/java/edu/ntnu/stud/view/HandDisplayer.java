package edu.ntnu.stud.view;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import edu.ntnu.stud.model.Hand;
import edu.ntnu.stud.model.PlayingCard;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class HandDisplayer extends GridPane {
    private AtomicReference<Hand> handRef;

    public HandDisplayer(AtomicReference<Hand> handRef) {
        this.handRef = handRef;
    }

    public void displayCards() {
        clearDisplay();
        Hand hand = handRef.get();
        List<PlayingCard> cards = hand.getHand();
        int column = 0;
        for (PlayingCard card : cards) {
            Label cardLabel = createCardLabel(card.getAsString());
            add(cardLabel, column++, 0);
        }
        setPadding(new Insets(10)); // Add padding around the grid
    }

    private void clearDisplay() {
        getChildren().clear(); // Clear all existing card labels
    }

    private Label createCardLabel(String cardText) {
        Label cardLabel = new Label(cardText);
        cardLabel.setStyle("-fx-border-color: black; -fx-padding: 40px;");
        return cardLabel;
    }
}