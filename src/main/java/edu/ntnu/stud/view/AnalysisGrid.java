package edu.ntnu.stud.view;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class AnalysisGrid extends GridPane {
    private TextField cardSumField;
    private TextField flushField;
    private TextField heartCardsField;
    private TextField spadeQueen;

    public AnalysisGrid() {
        setHgap(10); // Set horizontal gap between elements
        setVgap(20); // Set vertical gap between elements

        // Creating output elements
        Label cardSumLabel = new Label("Sum of the faces:");
        cardSumField = new TextField(""); // Create a TextField for editable output

        // Add output elements to the GridPane
        add(cardSumLabel, 0, 0); // column, row
        add(cardSumField, 1, 0);

        // Creating output elements
        Label flushLabel = new Label("Flush:");
        flushField = new TextField(""); // Create a TextField for editable output

        // Add output elements to the GridPane
        add(flushLabel, 0, 1);
        add(flushField, 1, 1);

        // Second set of output elements
        Label heartCardsLabel = new Label("Cards of heart:");
        heartCardsField = new TextField("");

        add(heartCardsLabel, 6, 0);
        add(heartCardsField,7, 0);

        Label spadeLabel = new Label("Queen of spades:");
        spadeQueen = new TextField("");

        add(spadeLabel,6 , 1);
        add(spadeQueen, 7, 1);
    }
    public TextField getSumField() {
        return cardSumField;
    }

    public TextField getFlushField() {
        return flushField;
    }

    public TextField getHCardsField() {
        return heartCardsField;
    }

    public TextField getSpadeQueen() {
        return spadeQueen;
    }
}