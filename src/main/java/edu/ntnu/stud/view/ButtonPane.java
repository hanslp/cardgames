package edu.ntnu.stud.view;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class ButtonPane extends VBox {
    private Button dealButton;
    private Button analyzeButton;

    public ButtonPane() {
        // Creating buttons
        dealButton = new Button("Deal hand");
        analyzeButton = new Button("Analyze hand");

        // Setting preferred sizes for buttons
        dealButton.setPrefSize(100, 100);
        analyzeButton.setPrefSize(100, 100);

        // Adding spacing between buttons
        setSpacing(50); // Adjust spacing as needed

        // Adding buttons to the VBox
        getChildren().addAll(dealButton, analyzeButton);
    }

    public Button getDealButton() {
        return dealButton;
    }

    public Button getAnalyzeButton() {
        return analyzeButton;
    }
}