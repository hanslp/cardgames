package edu.ntnu.stud.view;

import edu.ntnu.stud.model.Hand;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicReference;

public class CardGame extends Application {

    @Override
    public void start(Stage primaryStage) {
        AtomicReference<Hand> handRef = new AtomicReference<>(null);

        // Creating a Pane
        Pane root = new Pane();
        root.setPrefSize(1000, 800);
        root.setStyle("-fx-background-color: white;");

        // Creating a background area for the hand
        Rectangle handShowArea = new Rectangle(600, 500);
        handShowArea.setFill(Color.MEDIUMSEAGREEN); // Transparent fill color
        handShowArea.setStroke(Color.GRAY); // Border color
        handShowArea.setStrokeWidth(2); // Border width

        // Creating instances of the ButtonPane, AnalysisGrid and HandDisplayer
        ButtonPane buttonPane = new ButtonPane();
        AnalysisGrid analysisGrid = new AnalysisGrid();
        HandDisplayer handDisplayer = new HandDisplayer(handRef);

        // Positioning the HandShowArea
        handShowArea.setLayoutX(50);
        handShowArea.setLayoutY(50);

        // Positioning the ButtonPane
        buttonPane.setLayoutX(750);
        buttonPane.setLayoutY(100);

        // Positioning the OutputGridPane
        analysisGrid.setLayoutX(50);
        analysisGrid.setLayoutY(650);

        // Positioning the HandDisplayer
        double handDisplayerX = (80);
        double handDisplayerY = handShowArea.getY() + (handShowArea.getHeight() - handDisplayer.getMaxHeight()) / 2;
        handDisplayer.setLayoutX(handDisplayerX);
        handDisplayer.setLayoutY(handDisplayerY);

        root.getChildren().addAll(buttonPane, analysisGrid, handShowArea, handDisplayer);

        // Setting the action buttons
        buttonPane.getDealButton().setOnAction(event -> {
            handRef.set(new Hand(5));
            handDisplayer.displayCards();
        });

        buttonPane.getAnalyzeButton().setOnAction(event -> {
            Hand hand = handRef.get();
            if (hand == null) {
                return;
            }
            analysisGrid.getSumField().setText(String.valueOf(hand.getHandSum()));
            analysisGrid.getFlushField().setText(hand.checkFlush() ? "Yes" : "No");
            analysisGrid.getHCardsField().setText(hand.getHeartsInHand());
            analysisGrid.getSpadeQueen().setText(hand.containsQueenOfSpades() ? "Yes" : "No");
        });

        // Setting the Scene
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Card Game");
        primaryStage.show();
    }
}